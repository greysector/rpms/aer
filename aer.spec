%define _enable_debug_packages %{nil}
%define debug_package          %{nil}

Summary: Third person perspective action adventure and mystery game
Name: aer
Version: 1.0.4.2
Release: 1
URL: https://www.gog.com/game/aer
Group:  Applications/Games
Source0: https://cdn-hw.gog.com/secure/linux_offlines/1509324336/1509324336/52904509852000284/1899/aer_memories_of_old_1_0_4_2_34743.sh
Source1: aer.desktop
License: GOG
BuildRequires: bsdtar
ExclusiveArch: x86_64
Requires: libGL.so.1()(64bit)
Requires: libpulse-simple.so.0()(64bit)
Requires: libudev.so.1()(64bit)
Requires: libX11.so.6()(64bit)
Requires: libXcursor.so.1()(64bit)
Requires: libXext.so.6()(64bit)
Requires: libXinerama.so.1()(64bit)
Requires: libXi.so.6()(64bit)
Requires: libXrandr.so.2()(64bit)
Requires: libXxf86vm.so.1()(64bit)
Requires: mesa-dri-drivers%{_isa}

%global __provides_exclude_from ^%{_libdir}/%{name}/AER_Data/(Plugins|Mono)/x86_64/.*\\.so$

%description
The gods of old are forgotten, lost in the events that shattered the world,
leaving only fragments of islands in the sky. This mystic world of endless
skies, colorful islands and ancient ruins is in danger of falling into darkness.
As one of the last few shapeshifters, you are sent on a pilgrimage to the Land
of Gods. Uncover the secrets that will help save reality itself.

* Fly and explore by transforming into a bird at will
* A large open world of hidden secrets and new discoveries
* A pilgrimage of mysteries, puzzles and temples
* Strong focus on atmosphere and aesthetics with a vibrant minimalistic art
  style

%prep
%setup -qcT
bsdtar -x -f%{S:0}
rm -r data/noarch/game/{AER_Data/{Mono,Plugins}/x86,AER.x86}
find data/noarch/game/AER_Data -type f -print0 | xargs -0 chmod -x
chmod +x data/noarch/game/AER_Data/{Mono,Plugins}/x86_64/*.so
chmod -x data/noarch/game/ThirdPartyLicenses.txt

%install
install -dm755 %{buildroot}%{_libdir}/%{name}
cp -pr data/noarch/game/AER_Data %{buildroot}%{_libdir}/%{name}
install -pm755 data/noarch/game/AER.x86_64 %{buildroot}%{_libdir}/%{name}

install -Dpm644 data/noarch/support/icon.png %{buildroot}%{_datadir}/icons/hicolor/256x256/apps/%{name}.png
desktop-file-install --dir %{buildroot}%{_datadir}/applications %{S:1}

%files
%license "data/noarch/docs/End User License Agreement.txt"
%license data/noarch/game/ThirdPartyLicenses.txt
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/256x256/apps/%{name}.png
%{_libdir}/%{name}

%changelog
* Wed Feb 26 2020 Dominik Mierzejewski <rpm@greysector.net> 1.0.4.2-1
- initial build
